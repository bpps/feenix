<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since Twenty Seventeen 1.0
 * @version 1.0
 */

?>
<div class="page-wrapper <?php echo get_field('full_width_page') ? ' full-width-page' : ''; ?>">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php
		if ( has_post_thumbnail(get_the_id()) ) { ?>
			<div class="inner-banner banner">
				<div class="bnr-ctnt">
					<div class="banner-img-wrapper">
						<div class="banner-img">
							<img src="<?php echo get_the_post_thumbnail_url(get_the_id(), 'large'); ?>" alt="banner-img">
							<div class="ctr circle circle--counter"></div>
						</div>
						<div class="circle-line" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="800">
							<div class="circle circle--clock"></div>
							<div class="circle circle--counter"></div>
							<div class="circle circle--clock"></div>
						</div>
					</div>
					<div class="container">
						<div class="ctnt">
							<h1><?php the_title(); ?></h1>
						</div>
					</div>
				</div>
			</div>
		<?php
		} else {
			the_title( '<h1 class="entry-title">', '</h1>' );
		} ?>
		<div class="entry-content">
			<?php
				the_content();
				?>
		</div><!-- .entry-content -->
	</article><!-- #post-<?php the_ID(); ?> -->
</div>
