
	<footer>
		<div class="container">
    	<?php
      wp_nav_menu([ 'menu' => 'Footer Menu' ]);
	    if ( is_active_sidebar('footer-one') ) :
        dynamic_sidebar('footer-one');
	    endif; ?>
		</div>
	</footer>

	<?php wp_footer(); ?>
</body>

</html>
