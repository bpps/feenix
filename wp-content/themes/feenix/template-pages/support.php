<?php

/*

Template Name: support

*/

get_header();
global $post;

?>

<section class="contact-us care-contact-us">
  <div class="container">
    <div class="row">
      <div class="col-lg-7">
        <div class="contact-box">
          <div class="heading">
            <h3>Contact</h3>
          </div>
          <?php echo do_shortcode('[wpforms id="242" title="false"]'); ?>
        </div>
      </div>

      <div class="col-lg-5 right-map">
        <h3>Feenix Payments</h3>
        <ul>
          <li>1201 Broadway, Suite 701</li>
          <li>New York, NY 10001</li>
          <li><a href="tel:888 510 6305">(888) 510-6305</a></li>
          <li><a href="mailto:info@feenixpayment.com">info@feenixpayment.com</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>

<?php
get_footer(); ?>
