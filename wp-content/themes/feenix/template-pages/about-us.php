<?php

/*

Template Name: About-us

*/

get_header();
global $post; ?>

	<div class="inner-banner">
		<div class="bnr-ctnt">
			<div class="banner-img">
				<img src="<?php echo get_field('banner_image',$post->ID); ?>" alt="banner-img">
			</div>
			<div class="circle-img">
				<img src="<?php echo get_field('banner_circle_image',$post->ID); ?>" alt="banner-img">
			</div>
			<div class="container">
				<div class="ctnt">
					<h2><?php echo get_field('banner_text',$post->ID); ?></h2>
				</div>
			</div>
		</div>
	</div>

	<section class="about-us">
		<div class="container">
			<h3><?php echo get_field('who_we_are_heading',$post->ID); ?></h3>
			<?php
			if ( $content = get_field('who_we_are_content',$post->ID) ) {
				echo $content;
			} ?>
		</div>
	</section>

	<section class="middle-section">
		<div class="container">
			<?php
			if ( $heading = get_field('middle-section_heading',$post->ID) ) { ?>
				<h3 data-aos="fade-down" data-aos-easing="linear" data-aos-duration="200"><?php echo $heading; ?></h3>
			<?php
			}
			if ( $content = get_field('middle-section_content',$post->ID) ) { ?>
				<div data-aos="fade-down" data-aos-easing="linear" data-aos-duration="300">
					<?php echo $content; ?>
				</div>
			<?php
			} ?>
		</div>
	</section>

	<?php
	if ( $team = get_field('the_team_repeater',$post->ID) ) { ?>
		<section class="team-section">
			<h3><?php echo get_field('the_team_heading',$post->ID); ?></h3>
			<div class="container">
				<?php
			 	foreach ( $team as $team_value ) { ?>
					<article>
						<div class="row">
							<div class="col-md-6 col-12">
								<div class="title-img">
									<figure>
										<img src="<?php echo $team_value['team_member_image']; ?>" alt="team-1">
									</figure>
								</div>
							</div>
							<div class="col-md-6 col-12">
								<div class="title">
									<figure>
										<img src="<?php echo $team_value['single_image']; ?>" alt="team-1">
									</figure>
									<h4><?php echo $team_value['team_member_name']; ?></h4>
									<p><?php echo $team_value['content']; ?></p>
								</div>
							</div>
						</div>
					</article>
				<?php
				} ?>

			</div>
		</section>
	<?php
	}
get_footer(); ?>
