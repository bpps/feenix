<?php

/*

Template Name: web-solutions

*/

get_header();
global $post;

?>

<div class="inner-banner">
	<div class="bnr-ctnt">
		<div class="banner-img">
			<img src="<?php echo get_field('banner_image',$post->ID); ?>" alt="banner-img">
		</div>
		<div class="circle-img">
			<img src="<?php echo get_field('banner_circle_image',$post->ID); ?>" alt="banner-img">
		</div>
		<div class="container">
			<div class="ctnt">
				<h2><?php echo get_field('banner_text',$post->ID); ?></h2>
			</div>
		</div>
	</div>
</div>
<section class="team-section<?php echo get_field('alternate_backgrounds') ? '' : ' transparent'; ?>">
	<div class="container">
		<?php
		$team = get_field('middle_section_repeater',$post->ID);
		foreach ( $team as $team_value ) { ?>
			<article>
				<div class="row">
					<div class="col-md-6 col-12">
						<div class="title-img">
							<figure>
								<img src="<?php echo $team_value['title_image']; ?>" alt="services-1">
							</figure>
						</div>
					</div>
					<div class="col-md-6 col-12">
						<div class="title">
							<figure>
								<img src="<?php echo $team_value['single_image']; ?>" alt="team-1">
							</figure>
							<h4><?php echo $team_value['heading']; ?></h4>
							<p><?php echo $team_value['content']; ?></p>
						</div>
					</div>
				</div>
			</article>
		<?php
		} ?>

	</div>
</section>

<?php
get_footer(); ?>
