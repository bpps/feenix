<?php

/*

Template Name: services

*/

get_header();
global $post; ?>
<div class="inner-banner">
	<div class="bnr-ctnt">
		<div class="banner-img">
			<img src="<?php echo get_field('banner_image', $post->ID); ?>" alt="banner-img">
		</div>
		<div class="circle-img">
			<img src="<?php echo get_field('banner_circle_image', $post->ID); ?>" alt="banner-img">
		</div>
		<div class="container">
			<div class="ctnt">
				<h2><?php echo get_field('banner_text', $post->ID); ?></h2>
			</div>
		</div>
	</div>
</div>

<section class="team-section service-page">
	<div class="container">
		<?php
		$middle = get_field('middle_section_repeater', $post->ID);
		foreach ( $middle as $middle_value ) { ?>
			<article>
				<div class="row">
					<div class="col-md-6 col-12">
						<div class="title-img">
							<figure>
								<img src="<?php echo $middle_value['service_image']; ?>" alt="services-1">
							</figure>
						</div>
					</div>
					<div class="col-md-6 col-12">
						<div class="title">
							<figure>
								<img src="<?php echo $middle_value['single_image']; ?>" alt="team-1">
							</figure>
							<h4><?php echo $middle_value['heading']; ?></h4>
							<p><?php echo $middle_value['content']; ?></p>
						</div>
					</div>
				</div>
			</article>
		<?php
		} ?>
	</div>
</section>

<?php

get_footer();

?>
