<?php

/*

Template Name: Home

*/

get_header();
global $post;

?>

	<div class="banner">
		<div class="bnr-ctnt">
			<div class="banner-img-wrapper">
				<div class="banner-img">
					<img src="<?php echo get_field('banner_image',$post->ID); ?>" alt="banner-img">
					<div class="ctr circle circle--counter"></div>
				</div>
				<div class="circle-line" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="800">
					<div class="circle circle--clock"></div>
					<div class="circle circle--counter"></div>
					<div class="circle circle--clock"></div>
				</div>
			</div>
			<div class="container">
				<div class="ctnt">
					<h3><?php echo get_field('banner_title',$post->ID); ?></h3>
					<h1><?php echo get_field('banner_content',$post->ID); ?></h1>
					<?php
					if ( $button = get_field('banner_button', $post->ID) ) { ?>
						<a href="<?php echo $button['url']; ?>" class="btn">
							<?php echo $button['title']; ?>
						</a>
					<?php
					}
					if ( $sub_content = get_field('banner_sub_content', $post->ID) ) { ?>
						<div style="margin-top: 2rem;" class="home-banner-sub-content">
							<?php echo $sub_content; ?>
						</div>
					<?php
					} ?>
				</div>
			</div>
		</div>
	</div>


	<section class="middle-section">
		<div class="container">
			<h3 data-aos="fade-down" data-aos-easing="linear" data-aos-duration="200"><?php echo get_field('middle-section_heading',$post->ID); ?></h3>
			<p data-aos="fade-down" data-aos-easing="linear" data-aos-duration="300"><?php echo get_field('middle-section_content',$post->ID); ?></p>
			<div class="row">

			<?php
			$middle = get_field('middle-section_repeater',$post->ID);

			foreach($middle as $middle_value){

			?>
				<div class="col-md-4 col-12" data-aos="fade-right" data-aos-easing="linear" data-aos-duration="200">
					<div class="card-list">
						<figure>
							<img src="<?php echo $middle_value['images_']; ?>" alt="Card-1">
						</figure>
						<h4><?php echo $middle_value['text'];  ?></h4>
					</div>
				</div>
		<?php

			}

		?>

			</div>
		</div>
	</section>



	<section class="succeeds" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="300">
		<div class="container">
			<h4><?php echo get_field('feenix_succeeds_heading',$post->ID); ?></h4>
			<p><?php echo get_field('feenix_succeeds_content',$post->ID); ?></p>
			<?php
			if ( $brands = get_field('brands') ) { ?>
				<section class="brand">
					<div class="container">
						<div class="brand-slider">
							<?php
							foreach ( $brands as $logo ) { ?>
								<div>
									<div class="ctnt">
										<figure><img src="<?php echo $logo['url']; ?>" alt="<?php echo esc_attr($logo['alt']); ?>"></figure>
									</div>
								</div>
							<?php
							} ?>
						</div>
					</div>
				</section>
			<?php
			} ?>
		</div>
	</section>


	<section class="testimonial">
		<div class="testi-ctnt">
			<div class="slider-nav-thumbnails">


			<?php

			   $last = get_field('last_slider_repeater',$post->ID);
			   foreach($last as $last_value){


			 ?>

			    <div class="slide">
					<div class="banner-img">
						<img src="<?php echo $last_value['main_image']; ?>" alt="banner-img">
					</div>
				</div>

		    <?php

			       }

			?>

			  </div>


			<div class="circle-line">
				<div class="circle circle--clock"></div>
				<div class="circle circle--counter"></div>
				<div class="circle circle--clock"></div>
				<div class="circle circle--counter"></div>
				<div class="circle circle--clock"></div>
			</div>


		</div>

		<div class="container">
			<div class="tool">
				<div class="testi slider">

		<?php

			   $testimonials = get_field('last_slider_repeater',$post->ID);
			   foreach($testimonials as $testimonial){

		?>
				<div>
					<div class="ctnt">
						<p>
						  <?php echo $testimonial['content']; ?>
						</p>
					  <?php
						if ($testimonial['company'] ) { ?>
							<strong><?php echo $testimonial['company']; ?></strong>
						<?php
						}
						if ($testimonial['location'] ) { ?>
							<p><?php echo $testimonial['location']; ?></p>
						<?php
						} ?>
					</div>
				</div>

	<?php

	    }

	?>

				</div>
			</div>
		</div>
	</section>
<?php

get_footer();

?>
