<!doctype html>

<html>



<head>

	<meta charset="utf-8">

	<title>Feenix</title>

	<meta name="viewport"

		content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">

	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favico/favicon.ico" sizes="32x32" type="image/x-icon">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/images/favico/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/images/favico/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/images/favico/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/images/favico/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/images/favico/safari-pinned-tab.svg" color="#902433">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="theme-color" content="#ffffff">



<?php wp_head(); ?>



</head>


<?php
$body_class = '';
if ( is_page() ) {
	global $page;
	$body_class = get_field('white_background', $page->ID) ? 'bg-white' : '';
} ?>
<body class="<?php echo $body_class; ?>">



	<header>

		<div class="container">

			<div class="logo">

				<a class="navbar-brand" href="<?php echo get_site_url(); ?>">



				<?php

              $custom_logo_id = get_theme_mod( 'custom_logo' );

              $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );

                ?>



                <img src="<?php echo $image[0]; ?>" alt="logo">

				</a>



			</div>

			<nav class="navbar navbar-expand-lg">

				<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarFeenix"

					aria-controls="navbarFeenix" aria-expanded="false" aria-label="Toggle navigation">

					<span class="navbar-toggler-icon"></span>

				</button>

				<div class="collapse navbar-collapse" id="navbarFeenix">

				    <?php



                        wp_nav_menu(array(

                                      'menu'            =>  'Top Menu',

                                      'menu_class'      =>  'navbar-nav',

                                      'container'       =>  'div',


                        ));


				    ?>



					<div class="login-btn tp">

						<a href="https://portal.feenixpayment.com/" target="_blank" class="login-btn">Login</a>

					</div>

				</div>

			</nav>

		</div>

	</header>
