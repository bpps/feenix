import $ from 'jquery'
import 'slick-carousel'
import AOS from 'aos'

$(document).ready(function () {

  /** On Scroll Fixed Header **/
  $(window).scroll(function () {
    var scroll = $(window).scrollTop();

    if (scroll >= 50) {
      $("header").addClass("fixed");
    } else {
      $("header").removeClass("fixed");
    }
  });

  $(window).resize(function () {
    resizeHeader()
  })

  var didScroll;
  var lastScrollTop = 0;
  var delta = 1;
  var navbarHeight = $('header').outerHeight();

  $(window).scroll(function (event) {
    didScroll = true;
  });

  setInterval(function () {
    if (didScroll) {
      hasScrolled();
      didScroll = false;
    }
  }, 10);

  function hasScrolled() {
    var st = $(this).scrollTop();

    if (Math.abs(lastScrollTop - st) <= delta)
      return;

    if (st > lastScrollTop && st > navbarHeight) {

      $('header').removeClass('nav-down').addClass('nav-up');

    } else {
      if (st + $(window).height() < $(document).height()) {
        $('header').removeClass('nav-up').addClass('nav-down');
      }

    }

    lastScrollTop = st;
  }
  /** On Scroll Fixed Header End **/
  'use strict'
  resizeHeader()
  //Avoid pinch zoom on iOS
  document.addEventListener('touchmove', function (event) {
    if (event.scale !== 1) {
      event.preventDefault();
    }
  }, false);
  $('.slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    fade: false,
    asNavFor: '.slider-nav-thumbnails',

  });

  $('.slider-nav-thumbnails').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.slider',
    dots: false,
    arrows:false,
    //	centerMode: true,
    focusOnSelect: true
  });


  //remove active class from all thumbnail slides
  $('.slider-nav-thumbnails .slick-slide').removeClass('slick-active');

  //set active class to first thumbnail slides
  $('.slider-nav-thumbnails .slick-slide').eq(0).addClass('slick-active');

  // On before slide change match active thumbnail to current slide
  $('.slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    var mySlideNumber = nextSlide;
    $('.slider-nav-thumbnails .slick-slide').removeClass('slick-active');
    $('.slider-nav-thumbnails .slick-slide').eq(mySlideNumber).addClass('slick-active');
  });

  $('.brand-slider').slick({
    arrows: false,
    dots: false,
    speed: 300,
    slidesToShow: 6,
    arrows: true,
    slidesToScroll: 6,
    responsive: [
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
          infinite: true,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }
    ]
  });

  $(window).on('load', function() {
    var width = $(window).width();

    if (width < 991) {
      $('body').find('.login-btn.tp').detach().insertBefore('button.navbar-toggler.collapsed');
    } else {
      $('body').find('.login-btn.tp').detach().insertAfter('#navbarFeenix');
    }
  });

  $(document).ready(function(){
    $('button.navbar-toggler').click(function(){
      var navbar_obj = $($(this).data("target"));
      navbar_obj.toggleClass("open");
    });
  });
  $(document).ready(function() {
    $('li.menu-item-has-children > a').click(function(e) {
      if ( $(window).width() < 992 ) {
        e.preventDefault();
        $('.sub-menu').slideToggle("fast");
      }
    });
  })

  function resizeHeader () {
    var HeaderHeight = $("body").find("header").outerHeight();
    $("body").css("padding-top", HeaderHeight);
  }

})

AOS.init();
