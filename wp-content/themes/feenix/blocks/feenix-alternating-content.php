<section class="team-section<?php echo get_field('alternate_backgrounds') ? '' : ' transparent'; ?>">
  <?php
  if ( $heading = get_field('heading') ) { ?>
    <h3><?php echo $heading; ?></h3>
  <?php
  }
  if ( $c_blocks = get_field('content_blocks') ) { ?>
  	<div class="container">
  		<?php
  		foreach ( $c_blocks as $key => $c_block ) {
        $dir = $key % 2 == 0 ? 'left' : 'right'; ?>
  			<article data-aos="fade-<?php echo $dir; ?>" data-aos-easing="linear" data-aos-duration="400">
  				<div class="row">
  					<div class="col-md-6 col-12">
  						<div class="title-img">
  							<figure>
  								<img src="<?php echo $c_block['image']['sizes']['medium_large']; ?>" alt="capital-1">
  							</figure>
  						</div>
  					</div>
  					<div class="col-md-6 col-12">
  						<div class="title">
  							<figure>
  								<?php echo color_blocks(); ?>
  							</figure>
                <?php
                if ( $c_block['title']) { ?>
    							<h4 <?php if ($c_block['subtitle']) { echo 'style="margin-bottom: 0;"'; } ?>><?php echo $c_block['title']; ?></h4>
                <?php
                }
                if ( $c_block['subtitle'] ) { ?>
    							<h5 style="margin-bottom: .5rem;"><?php echo $c_block['subtitle']; ?></h5>
                <?php
                }
                if ( $c_block['content'] ) { ?>
    							<?php echo $c_block['content']; ?>
                <?php
                } ?>
  						</div>
  					</div>
  				</div>
  			</article>
  		<?php
  		} ?>
  	</div>
  <?php
  } ?>
</section>
