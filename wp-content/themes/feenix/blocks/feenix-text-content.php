<section class="feenix-text-section<?php echo get_field('dark_background') ? ' bg-dark-grey' : ''; ?>">
  <div class="container">
    <?php
    if ( $heading = get_field('text_heading') ) { ?>
      <h3 data-aos="fade-down" data-aos-easing="linear" data-aos-duration="200"><?php echo $heading; ?></h3>
    <?php
    }
    if ( $content = get_field('text_content') ) { ?>
      <div data-aos="fade-down" data-aos-easing="linear" data-aos-duration="300">
        <?php echo $content; ?>
      </div>
    <?php
    } ?>
  </div>
</section>
